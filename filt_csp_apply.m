function [y, w] = filt_csp_apply(x, coeff, dimm)
% CSP Common spatial pattern filter
% Spatial filtering function. Returns the filtered signal Y.
%
%   Use as:
%       y = filt_csp_apply(x, coeff, dimm)
%   Inputs:
%   x     [samples x channels]  the input matrix where M rows are the channels and N columns are the
%                               time bins
%
%   coeff [channels x channels] the entire P' matrix of filter coefficients [channels x samples] (see filt_csp_dec.m)
%
%   dimm                        the number of dimensions the filter should attemp to reduce the
%                               output vector space to
%
%   Otuputs:
%   y   [samples x patterns]    The filtered data by CSP
%   w   [patterns x dimm]       The weights of spatial patterns applied to
%                               the data
%
% SEE ALSO: filt_csp_dec.m

    %% Input check
    NumChannels = size(coeff, 1);

    if(NumChannels < dimm)
        error('chk:dim', 'Cannot reduce to a higher dimensional space!');
    end

    %% Instantiate filter matrix
    %PFilters = extract_filters(coeff, NumChannels, dimm);
    PFilters = coeff;
    
    %% Filtering

    w = PFilters';
    y = x*PFilters';
   
end

function filtmat = extract_filters(coeff, nchannels, dimm)
    
    filtmat = zeros(dimm, nchannels);
    
    %% Create the n-dimensional filter by sorting
    i=0;
    for d = 1:dimm
        if(mod(d,2)==0)   
            filtmat(d,:) = coeff(nchannels-i,:);
            i=i+1;
        else
            filtmat(d,:) = coeff(1+i,:);
        end
    end
end