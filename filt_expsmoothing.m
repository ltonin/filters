function dnew = filt_expsmoothing(alpha, d, dold)
    
    if nargin == 2
        dold = nan;
    end

    NumRows = size(d, 1);
    dnew = 0.5*ones(size(d));
    
    if (NumRows == 1) && (isnan(dold))
        error('ch:in', 'If d has a single row, dold is required');
    elseif (NumRows == 1)
        dnew = alpha * dold + (1 - alpha) * d;
    elseif (NumRows > 1)
        for sId = 2:NumRows
            dnew(sId, :) = alpha * dnew(sId - 1, :) + (1 - alpha) * d(sId, :);
            NumRows
        end
    end
end