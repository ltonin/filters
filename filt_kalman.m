function [y, Pnew, Xnew] = filt_kalman(ys, Pold, Xold, F, H, Q, R)
    %-------------------------------------------------------------------------%
    % KALMAN FILTER %
    %-------------------------------------------------------------------------%
    % ### INPUT ###
    % ys = value of the istant measurment
    % Pold = error extimation covariance matrix at the prior iteration
    % Xold = state vector at the prior iteration
    % F = state transition matrix
    % H = Input/Output trasfer matrix
    % Q = model error covariance matrix
    % R = measure error covariance matrix
    % ### OUTPUT ###
    % y = ys filtered value at the current iteration
    % Pnew = error extimation covariance matrix at the current iteration
    % Xnew = state vector at the current iteration
    %-------------------------------------------------------------------------%

    % predictive step of Kalman filter
    xpred=F*Xold;                               % calculation of x(t|t-1)
    Ppred=F*Pold*F'+Q;                          % calculation of P(t|t-1)
    K=Ppred*H'*inv(H*Ppred*H'+R);               % calculation of the filter gain currection

    % correction step using ys measurments
    e=ys-H*xpred;                               % prediction error
    Xnew=xpred+K*e;                             % calculation of x(t|t)
    I=eye(size(Q));
    Pnew=(I-K*H)*Ppred;                         % calculation of P(t|t)

    % calculation of the filtered value
    y=H*Xnew;

end