function handle = filt_highlow_stability(order, band, fs, type, family, decibels)
% handle = filt_highlow_stability(order, band, fs, type, [family, decibels])
%
% Plot the response of a given bandpass filter with order, band and fs given
% as argument.
%
% order     -- filter order
% band      -- vector with start and stop frequency for bandpass
% fs        -- sampling frequency of the signal
% type      -- filter type: high, low 
% family    -- filter family: butter, cheby1, cheby2 [default: butter] 
% decibels  -- gain in decibels (used only for cheby1 and cheby2)
%


    if nargin < 6
        family = 'butter';
        decibels = NaN;
    end
    
    switch family
        case 'butter'
            [b, a] =  butter(order,(band*2)/fs, type);
        case 'cheby1'
            [b, a] = cheby1(order, decibels, (band*2)/fs, type);
        case 'cheby2'
            [b, a] = cheby2(order, decibels, (band*2)/fs, type);
        otherwise
            error('chk:type', ['Unknown filter type: ' family]);
    end
    
    
    handle = fvtool(b, a, 'Fs', fs);



end