function [handle] = filt_bp_stability(order, band, fs, type, decibels)
% [handle] = filt_bp_stability(order, band, fs [type, decibels])
%
% Plot the response of a given bandpass filter with order, band and fs given
% as argument.
%
% order     -- filter order
% band      -- vector with start and stop frequency for bandpass
% fs        -- sampling frequency of the signal
% type      -- filter type: butter, cheby1, cheby2 [default: butter] 
% decibels  -- gain in decibels (used only for cheby1 and cheby2)
%
% SEE ALSO: filt_bp, plot_vline
    
    if nargin < 5
        type = 'butter';
        decibels = NaN;
    end
    
    switch type
        case 'butter'
            [b, a] = butter(order, (band*2)/fs);
        case 'cheby1'
            [b, a] = cheby1(order, decibels, (band*2)/fs);
        case 'cheby2'
            [b, a] = cheby2(order, decibels, (band*2)/fs);
        otherwise
            error('chk:type', ['Unknown filter type: ' type]);
    end
    
%     N = 10;
% 
%     [h, f] = freqz(b,a,fs*N,fs);
%     
%     handle = plot(f,abs(h),'.-');
%     
%     axis([0 3*band(2) 0 1]); 
%     xlabel('Hz');
%     ylim([0 1.5])
%     grid on
%     
%     plot_vline(band(1), 'r', num2str(band(1)));
%     plot_vline(band(2), 'r', num2str(band(2)));


    handle = fvtool(b, a, 'Fs', fs);
    
end
