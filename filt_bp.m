function data = filt_bp(s, order, band, fs, type, decibels)
% data = filt_bp(s, order, band, fs [type, decibels])
%
% The function bandpass the input signal s in a given frequency band.
%
% s         -- input signals points x channels
% order     -- filter order
% band      -- vector with start and stop frequency for bandpass
% fs        -- sampling frequency of the signal
% type      -- filter type: butter, cheby1, cheby2 [default: butter] 
% decibels  -- gain in decibels (used only for cheby1 and cheby2)
%
% The function returns the filtered data in the same format points x
% channels.
%
% N.B.: use filt_stability.m to check the stability of the filter
%
% SEE ALS0: filt_stability

    if nargin < 5
        type = 'butter';
        decibels = NaN;
    end
    
    switch type
        case 'butter'
            [b, a] = butter(order, (band*2)/fs);
        case 'cheby1'
            [b, a] = cheby1(order, decibels, (band*2)/fs);
        case 'cheby2'
            [b, a] = cheby2(order, decibels, (band*2)/fs);
        otherwise
            error('chk:type', ['Unknown filter type: ' type]);
    end
    
    
    data = zeros(size(s));
    
    for ChId = 1:size(s, 2)
        data(:, ChId) = filtfilt(b, a, s(:, ChId));
    end



end