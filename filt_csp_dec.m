function coeff = filt_csp_dec(x, labels)
% CSP Common spatial pattern decomposition
% The function decomposes the input signal x in CSP coefficients.
%
% Use as
%   coeff = filt_csp_dec(x, labels)
%
%   Input: 
%           data    [samples x channels]
%           labels  [1 x samples], data labeling.
%                   N.B. length(unique(labels)) = 2 !!!
%
%   Output:
%           coeff   [channels x channels], CSP decomposition coefficients
%
%
% This implements Ramoser, H., Gerking, M., and Pfurtscheller, G. "Optimal
% spatial filtering of single trial EEG during imagined hand movement."
% IEEE Trans. Rehab. Eng 8 (2000), 446, 441.
%
% The initial version was coded by James Ethridge and William Weaver.
% See http://www.mathworks.com/matlabcentral/fileexchange/22915-common-spatial-patterns
% Some cleanups by Robert Oostenveld, 2012
%
% SEE ALSO: filt_csp_apply.m

    %% Input check
    Classes     = unique(labels);
    NumClasses  = length(Classes);
    
    if (NumClasses ~= 2)
        error('chk:cls', 'Number of classes must be 2. length(unique(labels)) ~= 2!');
    end

    %% Reshape data
    x = x';
    
    dat1 = x(:, labels == Classes(1));
    dat2 = x(:, labels == Classes(2));

    %% Finding the covariance of each class and composite covariance
    R1 = dat1*dat1';
    R1 = R1/trace(R1);
    R2 = dat2*dat2';
    R2 = R2/trace(R2);

    %% Ramoser equation (2)
    Rsum = R1+R2;

    %% Find Eigenvalues and Eigenvectors of RC
    % Sort eigenvalues in descending order
    [EVecsum,EValsum] = eig(Rsum);
    [EValsum,ind] = sort(diag(EValsum),'descend');
    EVecsum = EVecsum(:,ind);

    %% Find Whitening Transformation Matrix - Ramoser Equation (3)
    W = sqrt(pinv(diag(EValsum))) * EVecsum';

    %% Whiten Data Using Whiting Transform - Ramoser Equation (4)
    S1 = W * R1 * W';
    S2 = W * R2 * W';

    %% Generalized eigenvectors/values
    [B,D] = eig(S1,S2);

    [~,ind]=sort(diag(D));
    B=B(:,ind);

    %% Resulting Projection Matrix-these are the spatial filter coefficients
    coeff = B'*W;
end